package org.noear.solon.auth.tags;

/**
 * @author noear
 * @since 1.4
 */
public interface Constants {
    static String ATTR_name = "name";
    static String ATTR_logical = "logical";

    static String TAG_authPermissions = "authPermissions";
    static String TAG_authRoles = "authRoles";

    static String TAG_permissions = "permissions";
    static String TAG_roles = "roles";

    static String PREFIX =  "auth";
}
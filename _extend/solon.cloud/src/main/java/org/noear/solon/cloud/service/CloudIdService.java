package org.noear.solon.cloud.service;

/**
 * @author noear
 * @since 1.3
 */
public interface CloudIdService {
    long generate();
}
